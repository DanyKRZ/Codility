﻿using System;
using Codility.Helpers;
using Codility.Lessons.CountingElements.MissingInteger;
using Xunit;

namespace Codility.Tests.Lessons.CountingElements.MissingInteger
{
    public class SmallestNonOccurredArrayElementFinderTests
    {
        [Theory]
        [InlineData(new int[] { 1, 3, 6, 4, 1, 2 }, 5)]
        [InlineData(new int[] { -1, -3, -6, 4, 1, 2 }, 3)]
        [InlineData(new int[] {-1, -3 }, 1)]
        [InlineData(new int[] { 1, 2, 3 }, 4)]
        [InlineData(new int[] { 0 }, 1)]
        [InlineData(new int[] { 1 }, 2)]
        [InlineData(new int[] { 2 }, 1)]
        [InlineData(new int[] { -1, -1, -1 }, 1)]
        [InlineData(new int[] { -1, 1, -1 }, 2)]
        [InlineData(new int[] {-71, 1, -71 }, 2)]
        [InlineData(new int[] { 100000, 100000}, 1)]
        public void Find_ReturnsSmallestNonOccurredElementOfArray(int[] array,
            int expectedElement)
        {
            var finder = new SmallestNonOccurredArrayElementFinder();

            var actualElement = finder.Find(array);

            Assert.Equal(expectedElement, actualElement);
        }

        [Theory]
        [InlineData(new int[] { -1000001, 8, 8, 7, 7 })]
        [InlineData(new int[] { 1000001, 8, 8, 7, 7 })]
        [InlineData(new int[] { })]
        public void Find_WithOutOfRangeParameters_ThrowsArgumentRangeException(int[] array)
        {
            var finder = new SmallestNonOccurredArrayElementFinder();

            Action find = () => finder.Find(array);

            Assert.Throws<ArgumentOutOfRangeException>(find);
        }

        [Fact]
        public void Find_OfOutOfRangeArrayLength_ThrowsArgumentRangeException()
        {
            var finder = new SmallestNonOccurredArrayElementFinder();
            var outOfRangeLength = 100001;
            var array = ArrayHelper.GenerateRandomArray(outOfRangeLength, 1000);

            Action shift = () => finder.Find(array);

            Assert.Throws<ArgumentOutOfRangeException>(shift);
        }

        [Fact]
        public void Find_WithNullParameter_ThrowsArgumentNullException()
        {
            var finder = new SmallestNonOccurredArrayElementFinder();

            Action find = () => finder.Find(null);

            Assert.Throws<ArgumentNullException>(find);
        }
    }
}

