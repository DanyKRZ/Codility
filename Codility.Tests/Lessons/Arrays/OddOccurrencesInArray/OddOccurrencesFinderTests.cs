﻿using System;
using Codility.Helpers;
using Codility.Lessons.Arrays.OddOccurrencesInArray;
using Xunit;

namespace Codility.Tests.Lessons.Arrays.OddOccurrencesInArray
{
    public class OddOccurrencesFinderTests
    {
        [Theory]
        [InlineData(new int[] { 9, 3, 9, 3, 9, 7, 9, 9, 3, 9, 3, 9, 7, 9, 9, 3, 9, 3, 9, 7, 9 }, 7)]
        [InlineData(new int[] { 9, 3, 9, 3, 1 }, 1)]
        [InlineData(new int[] { 1, 1, 2}, 2)]
        [InlineData(new int[] { 3, 3, 7, 7, 1000000000, 9, 9 }, 1000000000)]
        public void Find_ReturnsTheOddNumberInArray(int[] array,
            int expectedOddElement)
        {
            var finder = new OddOccurrencesFinder();

            var actualOddElement = finder.Find(array);

            Assert.Equal(expectedOddElement, actualOddElement);
        }

        [Theory]
        [InlineData(new int[] { -1000000001, 8, 8, 7, 7 })]
        [InlineData(new int[] { 1000000001, 8, 8, 7, 7 })]
        [InlineData(new int[] { 9, 3, 9, 3, -1 })]
        [InlineData(new int[] { 9, 3, 9, 3  })]
        [InlineData(new int[] { 9, 3, 9, 3, 0 })]
        [InlineData(new int[] { 1, 2, 3, 4, 5})]
        [InlineData(new int[] { })]
        public void Find_WithOutOfRangeParameters_ThrowsArgumentRangeException(int[] array)
        {
            var finder = new OddOccurrencesFinder();

            Action find = () => finder.Find(array);

            Assert.Throws<ArgumentOutOfRangeException>(find);
        }

        [Fact]
        public void Find_OfOutOfRangeArrayLength_ThrowsArgumentRangeException()
        {
            var finder = new OddOccurrencesFinder();
            var outOfRangeLength = 1000000001;
            var array = ArrayHelper.GenerateRandomArray(outOfRangeLength, 1000);

            Action shift = () => finder.Find(array);

            Assert.Throws<ArgumentOutOfRangeException>(shift);
        }

        [Fact]
        public void Find_WithNullParameter_ThrowsArgumentNullException()
        {
            var finder = new OddOccurrencesFinder();

            Action find = () => finder.Find(null);

            Assert.Throws<ArgumentNullException>(find);
        }
    }
}
