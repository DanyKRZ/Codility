﻿using System;
using Codility.Lessons.PrefixSums.CountDiv;
using Xunit;

namespace Codility.Tests.Lessons.PrefixSums.CountDiv
{
    public class DivisionCounterTests
    {
        [Theory]
        [InlineData(3, 16, 3, 5)]
        [InlineData(4, 4, 2, 1)]
        [InlineData(1, 16, 3, 5)]
        [InlineData(11, 345, 17, 20)]
        [InlineData(0, 11, 11, 2)]
        [InlineData(0, 0, 11, 1)]
        [InlineData(10, 10, 7, 0)]
        [InlineData(11, 14, 2, 2)] 
        [InlineData(11, 14400, 2, 7195)]
        [InlineData(5, 15, 5, 3)]
        [InlineData(0, 15, 5, 4)]
        [InlineData(5, 1575, 5, 315)]
        [InlineData(0, 1575, 5, 316)]
        [InlineData(1, 333, 3, 111)]
        [InlineData(101, 123456789, 10000, 12345)]
        [InlineData(0, 123, 3, 42)]
        public void Count_ReturnsDivisionNumber(int A, 
            int B, 
            int K, 
            int expectedDivisionNumber)
        {
            var counter = new DivisionCounter();

            var actualDivisionNumber = counter.Count(A,B,K);

            Assert.Equal(expectedDivisionNumber, actualDivisionNumber);
        }

        [Theory]
        [InlineData(4, 3, 3)]
        [InlineData(3, 3, 0)]
        [InlineData(-3, 3, 3)]
        [InlineData(3, 2000000001, 3)]
        [InlineData(2000000001, 2000000002, 3)]
        public void Count_WithOutOfRangeParameters_ThrowsArgumentOutOfRangeException(int A,
            int B,
            int K)
        {
            var counter = new DivisionCounter();

            Action count = () => counter.Count(A, B, K);

            Assert.Throws<ArgumentOutOfRangeException>(count);
        }        
    }
}
