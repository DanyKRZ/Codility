﻿using System;
using Codility.Lessons.PrefixSums.PassingCars;
using Xunit;

namespace Codility.Tests.Lessons.PrefixSums.PassingCars
{
    public class PassingCarsCounterTests
    {
        [Theory]
        [InlineData(new int[] { 0, 0 }, 0)]
        [InlineData(new int[] { 0, 1 }, 1)]
        [InlineData(new int[] { 1, 1 }, 0)]
        [InlineData(new int[] { 1, 0 }, 0)]
        [InlineData(new int[] { 0, 0, 0, 0 }, 0)]
        [InlineData(new int[] { 0, 0, 0, 1 }, 3)]
        [InlineData(new int[] { 0, 0, 1, 1 }, 4)]
        [InlineData(new int[] { 0, 0, 1, 0 }, 2)]
        [InlineData(new int[] { 0, 1, 0, 0 }, 1)]
        [InlineData(new int[] { 0, 1, 0, 1 }, 3)]
        [InlineData(new int[] { 0, 1, 1, 1 }, 3)]
        [InlineData(new int[] { 0, 1, 1, 0 }, 2)]
        [InlineData(new int[] { 0, 0, 0, 0, 1, 1 }, 8)]
        [InlineData(new int[] { 0, 1, 0, 1, 0, 1 }, 6)]
        [InlineData(new int[] { 1, 0, 1, 1, 0, 1 }, 4)]
        [InlineData(new int[] { 0, 1, 0, 0, 0, 0, 1, 1 }, 11)]
        [InlineData(new int[] { 0, 0, 0, 0, 1, 0, 1, 0, 1 }, 15)]
        [InlineData(new int[] { 0, 1, 0, 1, 1 }, 5)]
        [InlineData(new int[] { 0, 0, 0, 1, 1 }, 6)]
        [InlineData(new int[] { 0, 0, 0, 0, 1 }, 4)]
        [InlineData(new int[] { 0, 1, 1, 0, 1 }, 4)]
        [InlineData(new int[] { 0, 1, 1, 1, 1 }, 4)]
        [InlineData(new int[] { 0, 1, 0, 1, 1, 1 }, 7)]
        [InlineData(new int[] { 0, 0, 0, 1, 1, 1 }, 9)]
        [InlineData(new int[] { 0, 1, 1, 0, 1, 1 }, 6)]
        [InlineData(new int[] { 0, 1, 0, 1, 1, 0, 1, 0, 1, 1 }, 16)]
        [InlineData(new int[] { 0, 0, 0, 0, 0 }, 0)]
        [InlineData(new int[] { 1, 1, 1, 1, 1 }, 0)]
        public void Count_ReturnsCarsNumber(int[] cars,
            int expectedCarsNumber)
        {
            var counter = new PassingCarsCounter();

            var actualCarNumber = counter.Count(cars);

            Assert.Equal(expectedCarsNumber, actualCarNumber);
        }

        [Theory]
        [InlineData(new int[] { 0, 1, 0, 1, 2 })]
        [InlineData(new int[] { 0, 1, 0, 1, -1 })]
        [InlineData(new int[] {  })]
        public void Count_WithOutOfRangeParameters_ThrowsArgumentOutOfRangeException(int[] cars)
        {
            var counter = new PassingCarsCounter();

            Action count = () => counter.Count(cars);

            Assert.Throws<ArgumentOutOfRangeException>(count);
        }

        [Fact]
        public void Count_WithNullParameter_ThrowsArgumentNullException()
        {
            var counter = new PassingCarsCounter();

            Action count = () => counter.Count(null);

            Assert.Throws<ArgumentNullException>(count);
        }

        [Fact]
        public void Count_WithOutOfRangeArrayLength_ThrowsArgumentOutOfRangeException()
        {
            var counter = new PassingCarsCounter();
            int[] array = new int[100001];

            Action generate = () => counter.Count(array);

            Assert.Throws<ArgumentOutOfRangeException>(generate);
        }
    }
}
