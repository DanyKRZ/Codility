﻿using System;
using Codility.Helpers;
using Codility.Lessons.Sorting.MaxProductOfThree;
using Xunit;

namespace Codility.Tests.Lessons.Sorting.MaxProductOfThree
{
    public class MaxTripletFinderTests
    {
        [Theory]
        [InlineData(new int[] { -3, 1, 2, -2, 5, 6 }, 60)]
        [InlineData(new int[] { -5, -5, 5, 5 }, 125)]
        [InlineData(new int[] { -10, -2, -4 }, -80)]
        public void Find_ReturnsSmallestSliceStartingPosition(int[] array,
            int expectedMaxProduct)
        {
            var finder = new MaxTripletFinder();

            int actualMaxProduct = finder.Find(array);

            Assert.Equal(expectedMaxProduct, actualMaxProduct);
        }

        [Theory]
        [InlineData(new int[] { -1001, 1, 0 } )]
        [InlineData(new int[] { 1001, 1, 0 })]
        [InlineData(new int[] { 1001, 1 })]
        public void Find_WithOutOfRangeParameters_ThrowsArgumentRangeException(int[] array)
        {
            var finder = new MaxTripletFinder();

            Action find = () => finder.Find(array);

            Assert.Throws<ArgumentOutOfRangeException>(find);
        }

        [Fact]
        public void Find_WithNullParameter_ThrowsArgumentNullException()
        {
            var finder = new MaxTripletFinder();

            Action find = () => finder.Find(null);

            Assert.Throws<ArgumentNullException>(find);
        }

        [Fact]
        public void Find_WithOutOfRangeLength_ThrowsArgumentOutOfRangeException()
        {
            var finder = new MaxTripletFinder();
            var array = ArrayHelper.GenerateRandomArray(100001, 1000, -1000);

            Action find = () => finder.Find(array);

            Assert.Throws<ArgumentOutOfRangeException>(find);
        }


    }
}
