﻿using System;
using Codility.Lessons.TimeComplexity.MissingElementInArray;
using Xunit;

namespace Codility.Tests.Lessons.TimeComplexity.MissingElementInArray
{
    public class MissingElementFinderTests
    {
        [Theory]
        [InlineData(new int[] { 0, 2, 3 }, 1)]
        [InlineData(new int[] { 1, 0, 3 }, 2)]
        [InlineData(new int[] { 1, 2, 0 }, 3)]
        [InlineData(new int[] { 1, 9, 10, 11 }, 8)]
        [InlineData(new int[] { 8, 1, 10, 11 }, 9)]
        [InlineData(new int[] { 8, 9, 1, 11 }, 10)]
        [InlineData(new int[] { 8, 9, 10, 1 }, 11)]
        [InlineData(new int[] { 1, 9, 10, 11, 12 }, 8)]
        [InlineData(new int[] { 8, 1, 10, 11, 12 }, 9)]
        [InlineData(new int[] { 8, 9, 1, 11, 12 }, 10)]
        [InlineData(new int[] { 8, 9, 10, 1, 12}, 11)]
        [InlineData(new int[] { 8, 9, 10, 11, 1 }, 12)]
        [InlineData(new int[] { 1, 0}, 2)]
        [InlineData(new int[] { 0, 15 }, 1)]
        [InlineData(new int[] { 99999, 100000, 10000 }, 100001)]
        public void Find_ReturnsMissingElement(int[] arrayToFind,
            int expectedMissingElement)
        {
            var finder = new MissingElementFinder();

            var actualMissingElement = finder.Find(arrayToFind);

            Assert.Equal(expectedMissingElement, actualMissingElement);
        }

        [Theory]
        [InlineData(new int[] { 8, 9, 3, 11, 12, 1, 14 })]
        [InlineData(new int[] { 0, 1, 2, 7, 8, 9 })]
        [InlineData(new int[] { })]
        [InlineData(new int[] { 1, 5, 7, 9, 12 })]
        [InlineData(new int[] { 0, 1, 2, 3 })]
        [InlineData(new int[] { 0, 1, 2 })]
        [InlineData(new int[] { -1, 0})]
        [InlineData(new int[] { 99999, 100000, 100001, 3 })]
        [InlineData(new int[] { 0, 1 })]
        public void Find_WithOutOfRangeParameters_ThrowsArgumentRangeException(int[] array)
        {
            var finder = new MissingElementFinder();

            Action find = () => finder.Find(array);

            Assert.Throws<ArgumentOutOfRangeException>(find);
        }

        [Fact]
        public void Find_WithNullParameter_ThrowsArgumentNullException()
        {
            var finder = new MissingElementFinder();

            Action find = () => finder.Find(null);

            Assert.Throws<ArgumentNullException>(find);
        }
    }
}
