﻿using System;
using Codility.Lessons.TimeComplexity.PermMissingElem;
using Xunit;

namespace Codility.Tests.Lessons.TimeComplexity.PermMissingElem
{
    public class MissingElementFinderCodilityTests
    {
        [Theory]
        [InlineData(new int[] {  }, 1)]
        [InlineData(new int[] { 1 }, 2)]
        [InlineData(new int[] { 2 }, 3)]
        [InlineData(new int[] { 1, 2}, 3)]
        [InlineData(new int[] { 3, 2}, 4)]
        [InlineData(new int[] { 1, 2, 3, 4, 5}, 6)]
        [InlineData(new int[] { 2, 3, 4, 5, 6 }, 7)]
        [InlineData(new int[] { 4, 2, 3, 5, 1, 6, 8, 9 }, 7)]
        [InlineData(new int[] { 0, 1, 2, 3, 4, 5 }, 6)]
        public void Find_ReturnsMissingElement(int[] arrayToFind,
           int expectedMissingElement)
        {
            var finder = new MissingElementFinderCodility();

            var actualMissingElement = finder.Find(arrayToFind);

            Assert.Equal(expectedMissingElement, actualMissingElement);
        }

        [Theory]
        [InlineData(new int[] { 8, 9, 3, 11, 12, 1, 14 })]
        [InlineData(new int[] { 0, 1, 2, 7, 8, 9 })]
        [InlineData(new int[] { 1, 5, 7, 9, 12 })]
        [InlineData(new int[] { 99999, 100001, 100002 })]
        public void Find_WithOutOfRangeParameters_ThrowsArgumentRangeException(int[] array)
        {
            var finder = new MissingElementFinderCodility();

            Action find = () => finder.Find(array);

            Assert.Throws<ArgumentOutOfRangeException>(find);
        }

        [Fact]
        public void Find_WithNullParameter_ThrowsArgumentNullException()
        {
            var finder = new MissingElementFinderCodility();

            Action find = () => finder.Find(null);

            Assert.Throws<ArgumentNullException>(find);
        }
    }
}
