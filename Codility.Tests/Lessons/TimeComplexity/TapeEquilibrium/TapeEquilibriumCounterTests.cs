﻿using System;
using Codility.Helpers;
using Codility.Lessons.TimeComplexity.TapeEquilibrium;
using Xunit;

namespace Codility.Tests.Lessons.TimeComplexity.TapeEquilibrium
{
    public class TapeEquilibriumCounterTests
    {
        [Theory]
        [InlineData(new int[] { 3, 1, 2, 4, 3 }, 1)]
        [InlineData(new int[] { 3, 1 }, 2)]
        [InlineData(new int[] { 3, 1, 2 }, 0)]
        [InlineData(new int[] { 3, 1, 2, 5 }, 1)]
        [InlineData(new int[] { 3, 1, 2, 4, 3, 7 }, 0)]
        public void Count_ReturnsMinimalSumOfOppositeEquilibriumArrayParts(int[] array,
            int expectedMinimalSum)
        {
            var counter = new TapeEquilibriumCounter();
            
            var actualMinimalSum = counter.Count(array);

            Assert.Equal(expectedMinimalSum, actualMinimalSum);
        }

        [Theory]
        [InlineData(new int[] { -1001, 8, 8, 7, 7 })]
        [InlineData(new int[] { 1001, 8, 8, 7, 7 })]
        [InlineData(new int[] { 9 })]
        [InlineData(new int[] { })]
        public void Count_WithOutOfRangeParameters_ThrowsArgumentRangeException(int[] array)
        {
            var counter = new TapeEquilibriumCounter();

            Action count = () => counter.Count(array);

            Assert.Throws<ArgumentOutOfRangeException>(count);
        }

        [Fact]
        public void Count_OfOutOfRangeArrayLength_ThrowsArgumentRangeException()
        {
            var counter = new TapeEquilibriumCounter();
            var outOfRangeLength = 100001;
            var array = ArrayHelper.GenerateRandomArray(outOfRangeLength, 1000);

            Action shift = () => counter.Count(array);

            Assert.Throws<ArgumentOutOfRangeException>(shift);
        }

        [Fact]
        public void Count_WithNullParameter_ThrowsArgumentNullException()
        {
            var counter = new TapeEquilibriumCounter();

            Action count = () => counter.Count(null);

            Assert.Throws<ArgumentNullException>(count);
        }
    }
}
