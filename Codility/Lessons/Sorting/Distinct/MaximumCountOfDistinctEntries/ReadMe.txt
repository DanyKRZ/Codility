﻿Task description
Write a function

class Solution { public int solution(int[] A); }

that, given an array A consisting of N integers, returns the maximum number of repeated 
values in array A.

For example, given array A consisting of six elements such that:

 A[0] = 2    A[1] = 1    A[2] = 1
 A[3] = 2    A[4] = 3    A[5] = 1
the function should return 3, because
digit 1 appears 3 times in array A.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [0..100,000];
each element of array A is an integer within the range [−1,000,000..1,000,000].
