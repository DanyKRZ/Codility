﻿using System;
using System.Collections.Generic;
using System.Linq;
using Codility.Helpers;
using Codility.Testing;
using Codility.Validation;

namespace Codility.Lessons.Sorting.Distinct.MaximumCountOfDistinctEntries
{
    public class MaximumAppearanceCounter: Validator, ITestPrinter
    {
        public int Count(int[] A)
        {
            ValidateParameterForNull(A);
            ValidateArrayLengthRange(A.Length, 100000);
            ValidateElementForRange(A.Max(), 1000000, -1000000);
            ValidateElementForRange(A.Min(), 1000000, -1000000);

            if (A.Length == 0)
            {
                return 0;
            }

            Dictionary<int, int> valuesDictionary = new Dictionary<int, int>();

            foreach (int value in A)
            {
                int counter = 0;
                valuesDictionary.TryGetValue(value, out counter);
                valuesDictionary[value] = counter + 1;
            }

            return valuesDictionary.Values.Max();
        }

        public void PrintTest(TestType testType = TestType.Positive)
        {
            int[][] cases = null;

            switch (testType)
            {
                case TestType.Positive:
                    cases = new int[][] {
                        new int[] { 2, 1, 1, 3, 4, 1 },
                        ArrayHelper.GenerateRandomArray(100000, 1000000, -1000000),
                        ArrayHelper.GenerateRandomArray(15, 5),
                        ArrayHelper.GenerateRandomArray(15, 10, -10),
                        ArrayHelper.GenerateRandomArray(10, 7, 7)
                    };
                    break;
                case TestType.Negative:
                    cases = new int[][] {
                        ArrayHelper.GenerateRandomArray(100001, 1000000),
                        new int[] { 1, 1000001},
                        new int[] { 1, -1000001}
                    };
                    break;
            }

            foreach (var item in cases)
            {
                try
                {
                    ArrayHelper.PrintInitialArrayAndResult(item,
                        string.Format("The maximum number of appearances of the same digit in the initial array is {0}",
                                        Count(item)));
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine("\n{0}", ex.Message);
                }
            }
        }
    }
}
