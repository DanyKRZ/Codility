﻿using Codility.Validation;
using System;
using System.Linq;

namespace Codility.Lessons.Sorting.MaxProductOfThree
{
    public class MaxTripletFinder: Validator
    {
        public const int MinLength = 3;
        public const int MaxLength = 100000;

        public const int MinElement = -1000;
        public const int MaxElement = 1000;

        public int Find(int[] A)
        {
            ValidateParameterForNull(A);

            ValidateArrayLengthRange(A.Length, MaxLength, MinLength);

            ValidateElementForRange(A.Max(), MaxElement, MinElement);
            ValidateElementForRange(A.Min(), MaxElement, MinElement);

            int[] maxProducts = new int[3];
            int[] minProducts = new int[3];

            for (int i = 0; i < 3; i++)
            {
                maxProducts[i] = A[i];
                minProducts[i] = A[i];
            }

            Array.Sort(maxProducts);
            Array.Sort(minProducts);

            for (int i = 3; i < A.Length; i++)
            {
                if (A[i] > maxProducts[2])
                {
                    maxProducts[0] = maxProducts[1];
                    maxProducts[1] = maxProducts[2];
                    maxProducts[2] = A[i];
                }
                else if(A[i] > maxProducts[1])
                {
                    maxProducts[0] = maxProducts[1];
                    maxProducts[1] = A[i];
                }
                else if (A[i] > maxProducts[0])
                {
                    maxProducts[0] = A[i];
                }

                if (A[i] < minProducts[0])
                {
                    minProducts[0] = minProducts[1];
                    minProducts[1] = A[i];
                }
                else if(A[i] < minProducts[1])
                {
                    minProducts[1] = A[i];
                }
            }

            return Math.Max(maxProducts[0] * maxProducts[1] * maxProducts[2],
                            minProducts[0] * minProducts[1] * maxProducts[2]);
        }
    }
}
