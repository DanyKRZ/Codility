﻿using System;
using System.Linq;
using Codility.Helpers;
using Codility.Testing;
using Codility.Validation;

namespace Codility.Lessons.CountingElements.MaxCounters
{
    public class CountersArrayGenerator : Validator, ITestPrinter
    {
        public const int MaxLength = 100000;
        public const int MinLength = 1;

        public const int MinElement = 1;

        public int[] Generate(int N, int[] A)
        {
            ValidateParameterForNull(A);
            ValidateArrayLengthRange(A.Length, MaxLength, MinLength);
            int maxElement = N + 1;
            ValidateElementForRange(A.Max(), maxElement, MinElement);
            ValidateElementForRange(A.Min(), maxElement, MinElement);

            int[] newArray = new int[N];
            int generalMinimum = 0;
            int currentMinimum = 0;

            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] != maxElement)
                {
                    newArray[A[i] - 1] = Math.Max(newArray[A[i] - 1], generalMinimum);
                    newArray[A[i] - 1] += 1;

                    if (currentMinimum < newArray[A[i] - 1])
                    {
                        currentMinimum = newArray[A[i] - 1];
                    }
                }
                else
                {
                    generalMinimum = currentMinimum;
                }
            }

            for (int i = 0; i < newArray.Length; i++)
            {
                newArray[i] = Math.Max(newArray[i], generalMinimum);
            }

            return newArray;
        }

        public void PrintTest(TestType testType = TestType.Positive)
        {
            (int expectedLengthForGeneratedArray, int[] InitialArray)[] cases = null;

            switch (testType)
            {
                case TestType.Positive:
                    cases = new (int, int[])[] {
                        (20, new int[] { 21, 7, 14, 21, 14, 21, 18, 5, 5, 21, 14, 7, 6, 21, 6, 14, 18, 15, 4, 10, 19, 5, 10, 10, 12, 10, 17, 4, 16, 21}),
                        (100, new int [] {14, 32, 14, 51, 60, 85, 91, 99, 24, 28, 14, 3, 22, 101, 99, 2, 70, 19, 91, 64, 18, 10, 101, 68, 53, 42, 78, 28, 13, 47, 37, 101, 67, 12, 27, 101, 17, 101, 101, 77, 24, 42, 101, 40, 14, 4, 101, 13, 101, 101}),
                        (5, new int[] { 3, 4, 4, 6, 1, 4, 4 }),
                        (5, new int[] { 3, 4, 4, 4, 4, 4, 6 }),
                        (3, new int[] { 1, 3, 7 }),
                    };
                    break;
                case TestType.Negative:
                    cases = new (int, int[])[] {
                        (5, new int[] { 100000, 1 }),
                        (5, new int[] { 0, 1 }),
                        (5, new int[] { 1, 2, 3, 4, 5, 7 }),
                        (1, null)
                    };
                    break;
            }

            foreach (var item in cases)
            {
                try
                {
                    var generatedArray = Generate(item.expectedLengthForGeneratedArray, item.InitialArray);
                    Console.WriteLine(item.InitialArray);
                    Console.WriteLine("\n\n");
                    Console.WriteLine(generatedArray.ToString(" "));
                }
                catch (Exception ex)
                {
                    Console.WriteLine("\n{0}", ex.Message);
                }
            }
        }
    }
}
