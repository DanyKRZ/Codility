﻿using System;
using System.Linq;
using Codility.Helpers;
using Codility.Testing;
using Codility.Validation;

namespace Codility.Lessons.CountingElements.PermCheck
{    
    public class PermutationChecker: Validator, ITestPrinter
    {
        public const int MaxLength = 100000;
        public const int MinLength = 1;

        public const int MaxElement = 100000000;
        private const int MinElement = 1;  

        public int Check (int[] arrayToCheck)
        {
            ValidateParameterForNull(arrayToCheck);
            ValidateArrayLengthRange(arrayToCheck.Length, MaxLength, MinLength);
            ValidateElementForRange(arrayToCheck.Min(), MaxElement, MinElement);
            ValidateElementForRange(arrayToCheck.Max(), MaxElement, MinElement);           

            // Permutation is a sequence from 1 to arrayToCheck.Length
            if (arrayToCheck.Length == 1)
            {
                return arrayToCheck.First() == 1 ? 1 : 0;
            }

            Array.Sort(arrayToCheck);

            if (arrayToCheck.First() == 1)
            {
                bool isPermutation = true;

                for (int i = 0, j = arrayToCheck.Length - 1; i < arrayToCheck.Length / 2; i++, j--)
                {
                    if (arrayToCheck[j] - arrayToCheck[j - 1] != 1 || arrayToCheck[i + 1] - arrayToCheck[i] != 1)
                    {
                        isPermutation = false;
                    }
                }

                return isPermutation ? 1 : 0;
            }
            
            return 0;
        }

        public void PrintTest(TestType testType = TestType.Positive)
        {
            int[][] cases = null;

            switch (testType)
            {
                case TestType.Positive:
                    cases = new int[][] {
                        new int[] { 1 },
                        new int[] { 1, 2 },
                        new int[] { 1, 7 },
                        new int[] { 4, 1, 3, 2 },
                        new int[] { 9, 7, 5, 3, 2, 1, 4, 6, 10, 8, 11 },
                        new int[] { 100000000 },
                        new int[] { 2 },
                        new int[] { 4, 1, 3 },
                        new int[] { 1, 3 },
                        new int[] { 1, 4, 1  },                        
                    };
                    break;
                case TestType.Negative:
                    cases = new int[][] {
                        new int[] { 100000001, 1 },
                        new int[] { 0, 1 },
                        new int[] { 1, -1 },
                        null
                    };
                    break;
            }

            foreach (var item in cases)
            {
                try
                {
                    int result = Check(item);
                    ArrayHelper.PrintInitialArray(item);
                    Console.WriteLine( result > 0 ? 
                        "The sequance is a permutation" :
                        "The sequance is not a permutation");                    
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine("\n{0}", ex.Message);
                }
                catch (ArgumentNullException ex)
                {
                    Console.WriteLine("\n{0}", ex.Message);
                }
            }
        }
    }
}
