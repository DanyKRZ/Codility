﻿using System;
using System.Collections.Generic;
using System.Linq;
using Codility.Helpers;
using Codility.Testing;
using Codility.Validation;

namespace Codility.Lessons.CountingElements.MissingInteger
{
    public class SmallestNonOccurredArrayElementFinder : Validator, ITestPrinter
    {
        public int Find(int[] A)
        {
            ValidateParameterForNull(A);
            ValidateArrayLengthRange(A.Length, 100000, 1);
            ValidateElementForRange(A.Max(), 1000000, -1000000);
            ValidateElementForRange(A.Min(), 1000000, -1000000);

            int smallest = 1;

            if (A.Length > 1)
            {
                var distinctA = new List<int>();
                distinctA.AddRange(A.Distinct());

                var positiveElements = distinctA.Where(el => el > 0).ToList();                

                if (positiveElements.Count == 0)
                {
                    return smallest;
                }

                positiveElements.Sort();

                for (int i = 0; i < positiveElements.Count; i++, smallest++)
                {
                    if (positiveElements[i] > smallest)
                    {
                        return smallest;
                    }
                }
                return positiveElements.Last() + 1;
            }

            if (A.First() == smallest)
            {
                return smallest + 1;
            }

            return smallest;
        }

        public void PrintTest(TestType testType = TestType.Positive)
        {
            int [][] cases = null;            

            switch(testType)
            {
                case TestType.Positive:
                    cases = new int[][] {
                            new int[] { 1, 3, 6, 4, 1, 2 },
                            new int[] { -1, -3, -6, 4, 1, 2 },
                            new int[] { -1, -3 },
                            new int[] { 1, 2, 3 },
                            new int[] { 0 },
                            new int[] { 1 },
                            new int[] { 2 },
                            new int[] { -1, -1, -1 },
                            new int[] { -1, 1,-1 },
                            new int[] { -71, 1, -71 },
                            new int[] { 100000, 100000 } };
                break;
                case TestType.Negative:
                    cases = new int[][] {
                        new int[] { },
                        ArrayHelper.GenerateRandomArray(100001, 1000000),
                        new int[] { 1, 1000001},
                        new int[] { 1, -1000001} 
                    };
                break;
            }            

            foreach (var item in cases)
            {
                try
                {
                    ArrayHelper.PrintInitialArrayAndResult(item,
                        string.Format("The smallest positive integer (greater than 0) that does not occur in A is {0}",
                        Find(item)));
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine("\n{0}", ex.Message);
                }
            }
        }
    }
}

