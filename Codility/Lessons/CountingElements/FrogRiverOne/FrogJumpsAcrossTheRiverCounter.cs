﻿using System;
using System.Collections.Generic;
using System.Linq;
using Codility.Helpers;
using Codility.Testing;
using Codility.Validation;

namespace Codility.Lessons.CountingElements.FrogRiverOne
{
    public class FrogJumpsAcrossTheRiverCounter : Validator, ITestPrinter
    {
        private const int MaximumRange = 100000;
        private const int MinimumRange = 1;

        public int Count(int X, int[] A)
        {
            ValidateParameterForNull(A);
            ValidateArrayLengthRange(A.Length, MaximumRange, MinimumRange);
            int max = A.Max();
            ValidateElementForRange(X, MaximumRange, MinimumRange);
            ValidateElementForRange(max, X, MinimumRange);
            ValidateElementForRange(A.Min(), X, MinimumRange);

            // Frogs can jump only when leaves appear at every position (i.e 1-X)
            // oposite bank of river is X+1           

            if (max < X)
            {
                return -1;
            }

            // Key = position, Value = Second
            Dictionary<int, int> results = new Dictionary<int, int>();

            for (int i = 0, j = A.Length - 1; i < A.Length ; i++, j--)
            {
                results.TryAdd(A[i], i);
            }            

            return results.Count < X ? -1 : results.Last().Value;
        }

        public void PrintTest(TestType testType = TestType.Positive)
        {
            (int LastRiverPosition, int[] Leaves)[] cases = null;
            switch (testType)
            {
                case TestType.Positive:
                    cases = new (int, int[])[]
                    {
                        (5, new int[] { 1, 3, 1, 4, 2, 3, 5, 4 }),
                        (5, new int[] { 1, 3, 1, 4, 2, 3, 4, 4 }),
                        (5, new int[] { 5, 4, 3, 4, 3, 2, 1 } ),
                        (1, new int[] { 1 }),
                        (100000, new int[] { 5, 4, 3, 4, 3, 2, 1 }),
                        (100000, new int[] { 5, 4, 100000, 4, 3, 2, 1 })
                    };
                    break;

                case TestType.Negative:
                    cases = new (int, int[])[] {
                        (100001, new int[] { 1, 3, 1, 4, 2, 3, 5, 4 }),
                        (100001, new int[] { 1, 3, 100001, 4, 2, 3, 4, 4 }),
                        (5, new int[] { 1, 3, 1, 4, 100001, 3, 5, 4 }),
                        (0, new int[] { 5, 5, 3, 4, 3, 2, 1 }),
                        (5, new int[] { 5, 5, 3, 0, 3, 2, 1 })
                    };

                    break;
            }

            PrintTheStartOfTheStory();

            foreach (var item in cases)
            {
                try
                {
                    int result = Count(item.LastRiverPosition, item.Leaves);
                    ArrayHelper.PrintInitialArray(item.Leaves);
                    PrintResult(item.LastRiverPosition, result);
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine("\n{0}", ex.Message);
                }
            }
        }

        private void PrintResult(int lastRiverPosition, int result)
        {            
            Console.WriteLine("\nThe width of river is {0}", lastRiverPosition);

            if (result < 0)
            {
                Console.WriteLine("Oooops, there aren't enough leaves for the frog to jump across the river (the result is -1).");
            }
            if (result == 0)
            {
                Console.WriteLine("The reaver is so small and the frog doesn't need to wait (the result is 0 seconds).");
            }
            if (result > 0)
            {
                Console.WriteLine("The small frog needs to wait {0} seconds", result);
            }
        }

        private void PrintTheStartOfTheStory()
        {
            Console.WriteLine("The small frog needs to jump across the river.");
            Console.WriteLine("She may do it only with the help of leaves, that are fallen on the water");
            Console.WriteLine("Each leave falls on the water every second.");
        }

    }
}
