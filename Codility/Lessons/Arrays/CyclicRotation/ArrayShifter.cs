﻿using System;
using System.Linq;
using Codility.Helpers;
using Codility.Testing;
using Codility.Validation;

namespace Codility.Lessons.Arrays.CyclicRotation
{
    public class ArrayShifter: Validator, ITestPrinter
    {
        public int[] Shift(int[] A, int K)
        {
            ValidateParameterForNull(A);
            ValidateArrayLengthRange(A.Length, 100);
            ValidateElementForRange(K, 100);

            if (A.Length > 0)
            {
                ValidateElementForRange(A.Min(), 1000, -1000);
                ValidateElementForRange(A.Max(), 1000, -1000);
            }

            if (K > 0)
            {
                int[] arrayToShift = new int[A.Length];
                int[] shiftedArray = new int[A.Length];

                for (int i = 0; i < K; i++)
                {
                    if (i == 0)
                    {
                        arrayToShift = A;
                    }
                    else
                    {
                        arrayToShift = shiftedArray;
                    }

                    shiftedArray = Shift(arrayToShift);
                }

                return shiftedArray;
            }

            return A;        
        }

        public void PrintTest(TestType testType = TestType.Positive)
        {
            (int[] Array, int ShiftingNumber)[] cases = null;

            switch (testType)
            {
                case TestType.Positive:
                    cases = new (int[], int)[] {
                        (new int[] { 3, 8, 9, 7, 6}, 1),
                        (new int[] { 3, 8, 9, 7, 6}, 2),
                        (new int[] { 3, 8, 9, 7, 6}, 3),
                        (new int[] { 1, 2, 3, 4 }, 4) ,
                        (new int[] { 0, 0, 0 }, 3),
                        (new int[] { 1, 0 }, 2),
                        (new int[] { -4 }, 0),
                        (new int[] { }, 0),
                    };
                    break;

                case TestType.Negative:
                    cases = new (int[], int)[] {
                        (new int[] { 3, 8, 9, 7, 6}, -1),
                        (new int[] { 3, -8, 9, 7, -6}, 101),
                        (new int[] { 
                            3, 9, 7, 7, 3, 9, 7, 7, 0, 1,
                            3, 9, 7, 7, 3, 9, 7, 7, 0, 1,
                            3, 9, 7, 7, 3, 9, 7, 7, 0, 1,
                            3, 9, 7, 7, 3, 9, 7, 7, 0, 1,
                            3, 9, 7, 7, 3, 9, 7, 7, 0, 1,
                            3, 9, 7, 7, 3, 9, 7, 7, 0, 1,
                            3, 9, 7, 7, 3, 9, 7, 7, 0, 1,
                            3, 9, 7, 7, 3, 9, 7, 7, 0, 1,
                            3, 9, 7, 7, 3, 9, 7, 7, 0, 1,
                            3, 9, 7, 7, 3, 9, 7, 7, 0, 1, 11},
                            2),
                        (new int[] { -1001, 8, 9, 7, 6}, 1),
                        (new int[] { 1001, 8, 9, 7, 6}, 1)

                    };
                    break;
            }

            foreach (var item in cases)
            {
                try
                {
                    ShiftAndPrint(item.Array, item.ShiftingNumber);
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine("\n{0}", ex.Message);
                }
            }
        }

        private int[] Shift (int[] A)
        {
            int[] shiftedArray = new int[A.Length];

            for (int i = 0; i < A.Length; i++)
            {
                if (i == 0)
                {
                    shiftedArray[i] = A[A.Length - 1];
                }
                else
                {
                    shiftedArray[i] = A[i-1];
                }
            }
            return shiftedArray;
        }

        private void ShiftAndPrint(int[] initialArray, int timesToShift)
        {
            Console.WriteLine("The initial array should be shifted {0} times: ", timesToShift);
            initialArray.Print();

            Console.WriteLine("\nThe shifted array: ");
            Console.WriteLine($"{Shift(initialArray, timesToShift).ToString(" ")}\n\n");
        }
    }
}
