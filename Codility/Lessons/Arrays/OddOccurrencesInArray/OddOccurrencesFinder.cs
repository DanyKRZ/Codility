﻿using System;
using System.Linq;
using Codility.Helpers;
using Codility.Testing;
using Codility.Validation;

namespace Codility.Lessons.Arrays.OddOccurrencesInArray
{    
    public class OddOccurrencesFinder: Validator, ITestPrinter
    {
        public int Find (int[] A)
        {
            ValidateParameterForNull(A);
            ValidateArrayLengthRange(A.Length, 1000000, 1);
            ValidateArrayLengthForOdd(A.Length, true);
            ValidateElementForRange(A.Min(), 1000000000, 1);
            ValidateElementForRange(A.Max(), 1000000000, 1);
            ValidateOddOccurancies(A);

            if (A.Length > 1)
            {
                Array.Sort(A);
                for (int i = 0, j = A.Length - 1; i < A.Length - 2; i += 2, j -= 2)
                {
                    if (A[i] != A[i + 1])
                    {
                        if (A[i + 2] == A[i + 1])
                        {
                            return A[i];
                        }
                    }

                    if (A[j] != A[j - 1])
                    {
                        if (A[j - 1] == A[j - 2])
                        {
                            return A[j];
                        }
                    }
                }
            }

            return A[0];
        }

        public void PrintTest(TestType testType = TestType.Positive)
        {
            int[][] cases = null;

            switch (testType)
            {
                case TestType.Positive:
                    cases = new int[][] {
                        new int[] { 9, 3, 9, 3, 9, 7, 9, 9, 3, 9, 3, 9, 7, 9, 9, 3, 9, 3, 9, 7, 9},
                        new int[] { 9, 3, 9, 3, 1},
                        new int[] { 1, 1, 2},
                        new int[] { 3, 3, 7, 7, 7, 9, 9}
                    };
                    break;
                case TestType.Negative:
                    cases = new int[][] {
                        ArrayHelper.GenerateRandomArray(1000001, 2),
                        new int[] { 1, 1000000001},
                        new int[] { 1, 1, -1},
                        new int[] { 1, 1 },
                        new int[] { 1, 2, 9},
                        new int[] { 1, 2, 1, 3, 9}
                    };
                    break;
            }

            foreach (var item in cases)
            {
                try
                {
                    ArrayHelper.PrintInitialArray(item);
                    Console.WriteLine(Find(item));
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine("\n{0}", ex.Message);
                }
            }
        }

        private void ValidateOddOccurancies(int[] A)
        {
            var distictValues = A.Distinct().ToList();
            int distinctOccuranciesCounter = 0;

            for (int i = 0; i < distictValues.Count; i++)
            {
                if (A.Where(x => x == distictValues[i]).Count() % 2 != 0)
                {
                    distinctOccuranciesCounter++;
                }
                if (distinctOccuranciesCounter > 1)
                {
                    throw new ArgumentOutOfRangeException(nameof(A),
                    "According to the task assumptions only one value in A occur an even number of times");
                }
            }
        }
    }
}
