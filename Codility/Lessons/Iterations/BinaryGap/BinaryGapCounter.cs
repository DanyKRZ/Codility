﻿using Codility.Testing;
using Codility.Validation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Codility.Lessons.Iterations.BinaryGap
{
    public class BinaryGapCounter: Validator, ITestPrinter
    {
        public int Count(int N)
        {
            ValidateElementForRange(N, 2147483647, 1);

            string binaryString = Convert.ToString(N, 2);
            int gap = 0;
            List<int> gaps = new List<int>();
            bool openedGap = false;

            for (int i = 1; i < binaryString.Length - 1; i++)
            {
                if (openedGap && binaryString[i - 1] == '0' && binaryString[i] == '0' && binaryString[i + 1] == '0')
                {
                    gap++;
                }
                if (binaryString[i - 1] == '1' && binaryString[i] == '0' && binaryString[i + 1] == '0')
                {
                    gap += 2;
                    openedGap = true;
                }
                if (binaryString[i - 1] == '0' && binaryString[i] == '0' && binaryString[i + 1] == '1')
                {
                    gaps.Add(gap);
                    gap = 0;
                    openedGap = false;
                }
                if (binaryString[i - 1] == '1' && binaryString[i] == '0' && binaryString[i + 1] == '1')
                {
                    gaps.Add(1);
                }
            }

            return gaps.Count() > 0 ? gaps.Max() : 0;
        }

        public void PrintTest(TestType testType = TestType.Positive)
        {
            int[] cases = null;

            switch(testType)
            {
                case TestType.Positive:
                    cases = new int[] { 9, 10, 169, 17, 33, 65, 69, 1, 1041, 2147483647, 5 };
                    break;
                case TestType.Negative:
                    cases = new int[] { -1, 0 };
                break;
            }

            foreach (var item in cases)
            {
                try
                {
                    PrintResult(item, Count(item));
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine("\n{0}", ex.Message);
                }
            }
        }   

        private void PrintResult(int decimalDigit, int binaryGap)
        {
            Console.WriteLine("The binary representation of number {0} is {1}", 
                decimalDigit, Convert.ToString(decimalDigit, 2));
            Console.WriteLine("The maximum binary gap is: {0}\n", binaryGap);
        }
    }
}