﻿using System;
using System.Linq;
using Codility.Validation;

namespace Codility.Lessons.PrefixSums.GenomicRangeQuery
{
    public class MinimalImpactFactorFinder : Validator
    {
        public const int MinStringLength = 1;
        public const int MaxStringLength = 100000;
        
        public const int MaxElementLength = 50000;

        public readonly char[] PossibleNucleotides = new char[] { 'A', //1
            'C',
            'G',
            'T' };

        public int[] Find(string S, int[] P, int[] Q)
        {
            ValidateParameterForNull(P);
            ValidateParameterForNull(Q);
            ValidateParameterForNull(S);

            ValidateNucleotidesString(S);

            ValidateArrayLengthRange(S.Length, MaxStringLength, MinStringLength);
            ValidateArrayLengthRange(P.Length, MaxElementLength, MinStringLength);
            ValidateArrayLengthRange(Q.Length, P.Length, P.Length);           

            ValidateElementForRange(P.Max(), S.Length - 1);
            ValidateElementForRange(P.Min(), S.Length - 1);
            ValidateElementForRange(Q.Max(), S.Length - 1);
            ValidateElementForRange(Q.Min(), S.Length - 1);
            
            for (int i = 0; i < P.Length; i++)
            {
                if (P[i] > Q[i])
                {
                    throw new ArgumentOutOfRangeException(nameof(P));
                }
            }
            
            int [,] genomes = new int[3,S.Length + 1];
            int[] minimalImpactFactors = new int[P.Length];

            for (int i = 0; i < S.Length; i++)
            {
                genomes[0, i + 1] = genomes[0, i] + (S[i] == 'A' ? 1 : 0);
                genomes[1, i + 1] = genomes[1, i] + (S[i] == 'C' ? 1 : 0);
                genomes[2, i + 1] = genomes[2, i] + (S[i] == 'G' ? 1 : 0);
            }

            for (int i = 0; i < P.Length; i++)
            {
                if (genomes[0, Q[i] + 1] - genomes[0, P[i]] > 0)
                {
                    minimalImpactFactors[i] = 1;
                }
                else if (genomes[1, Q[i] + 1] - genomes[1, P[i]] > 0)
                {
                    minimalImpactFactors[i] = 2;
                }
                else if (genomes[2, Q[i] + 1] - genomes[2, P[i]] > 0)
                {
                    minimalImpactFactors[i] = 3;
                }
                else
                {
                    minimalImpactFactors[i] = 4;
                }
            }
            
            return minimalImpactFactors;
        }

        private void ValidateNucleotidesString(string S)
        {
            if (!S.All(c => PossibleNucleotides.Contains(c)))
            {
                throw new ArgumentOutOfRangeException(nameof(S), S);
            }
        }

    }
}
