﻿using System.Linq;
using Codility.Validation;

namespace Codility.Lessons.PrefixSums.PassingCars
{
    public class PassingCarsCounter : Validator
    {
        public const int MinElement = 0;
        public const int MaxElement = 1;

        public const int MinLength = 1;
        public const int MaxLength = 100000;

        public const int MaxPairsNumber = 1000000000;
        public int Count (int[] A)
        {
            ValidateParameterForNull(A);
            ValidateArrayLengthRange(A.Length, MaxLength, MinLength);
            ValidateElementForRange(A.Min(), MaxElement, MinElement);
            ValidateElementForRange(A.Max(), MaxElement, MinElement);

            int pairsCounter = 0;
            int zeroCounter = 0;

            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] == MinElement)
                {
                    zeroCounter++;
                }
                else
                {
                    pairsCounter += zeroCounter;

                    if (pairsCounter > MaxPairsNumber)
                    {
                        return -1;
                    }
                }
            }

            return pairsCounter;
        }
    }
}