﻿using Codility.Validation;

namespace Codility.Lessons.PrefixSums.CountDiv
{
    public class DivisionCounter : Validator
    {
        public const int MinElement = 1;
        public const int MaxElement = 2000000000;

        public int Count(int A, int B, int K)
        {
            ValidateElementForRange(B, MaxElement);
            ValidateElementForRange(A, B);
            ValidateElementForRange(K, MaxElement, MinElement);

            int divisionCounter = 0;

            if (A == 0 || A % K == 0 || (B - A) % K > 0)
            {
                divisionCounter++;
            }

            divisionCounter += (B - A) / K;

            if (K * divisionCounter > B && A != 0 && A % K != 0)
            {
                divisionCounter--;
            }           
                        
            return divisionCounter;
        }
    }
}