﻿using System;
using System.Collections.Generic;
using System.Linq;
using Codility.Helpers;
using Codility.Testing;
using Codility.Validation;

namespace Codility.Lessons.TimeComplexity.PermMissingElem
{
    public class MissingElementFinderCodility : Validator, ITestPrinter
    {
        public int Find(int[] A)
        {
            ValidateParameterForNull(A);

            if (A.Length == 0)
            {
                return 1;
            }

            Array.Sort(A);

            ValidateElementForRange(A.Max(), 100001);
            ValidateElementForRange(A.Min(), 100001);
            ValidateDistinctElements(A);

            var missingElements = new List<int>();
            bool found = false;

            if (A.Length == 2)
            {
                if (A[1] - A[0] != 1)
                {
                    return A[0] + 1;
                }
            }

            for (int i = 0, j = A.Length - 1; i < A.Length / 2; i++, j--)
            {
                if (A[j] - A[j - 1] != 1)
                {
                    found = true;

                    if (A[j - 1] - A[j - 2] == 1)
                    {
                        missingElements.Add(A[j - 1] + 1);
                    }
                    else
                    {
                        missingElements.Add(A[j] - 1);
                    }
                }

                if (A[i + 1] - A[i] != 1)
                {
                    found = true;

                    if (A[i + 2] - A[i + 1] == 1)
                    {
                        missingElements.Add(A[i + 1] - 1);
                    }
                    else
                    {
                        missingElements.Add(A[i + 2] - 1);
                    }
                }
            }

            if (!found)
            {
                return A.Last() + 1;
            }

            if (missingElements.Distinct().Count() > 1)
            {
                throw new ArgumentOutOfRangeException(nameof(A),
                      A.ToString(", "),
                     "According to the task assumptions each element of array A is an integer within the range [1..(N + 1)].");
            }

            return missingElements.FirstOrDefault();
        }

        public void PrintTest(TestType testType = TestType.Positive)
        {
            int[][] cases = null;

            switch (testType)
            {
                case TestType.Positive:
                    cases = new int[][] {
                        new int[] { },
                        new int[] { 1 },
                        new int[] { 2 },
                        new int[] { 1, 2 },
                        new int[] { 3, 2 },
                        new int[] { 1, 2, 3, 4, 5 },
                        new int[] { 2, 3, 4, 5, 6 },
                        new int[] { 4, 2, 3, 5, 1, 6, 8, 9 },
                        new int[] { 0, 1, 2, 3, 4, 5 }
                    };
                    break;

                case TestType.Negative:
                    cases = new int[][] {
                        new int[] { 8, 9, 3, 11, 12, 1, 14},
                        new int[] { 0, 1, 2, 7, 8, 9 },
                        new int[] { 1, 5, 7, 9, 12 },
                        new int[] { -1, 0 },
                        new int[] { 99999, 100000, 100001, 3 }
                    };
                    break;
            }

            foreach (var item in cases)
            {
                try
                {
                    ArrayHelper.PrintInitialArrayAndResult(item,
                       string.Format("Missing element is {0}",
                                      Find(item)));
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine("\n{0}", ex.Message);
                }
            }
        }
    }
}
