﻿using System;
using Codility.Testing;
using Codility.Validation;

namespace Codility.Lessons.TimeComplexity.FrogJmp
{
    public class FrogJumpsAcrossTheRoadCounter : Validator, ITestPrinter
    {
        public const int MinElement = 1;
        public const int MaxElement = 1000000000;

        public int Count(int startPosition, int desiredPosition, int stepSize)
        {
           ValidateElementForRange(startPosition, MaxElement, MinElement);
           ValidateElementForRange(desiredPosition, MaxElement, MinElement);
           ValidateElementForRange(stepSize, MaxElement, MinElement);

           ValidateParameters(startPosition, desiredPosition);

           int stepsCounter = 0;

           if (startPosition == desiredPosition)
           {
               return stepsCounter;
           }

           var stepDifference = desiredPosition - startPosition;
           stepsCounter = stepDifference / stepSize;
            
           if (stepDifference % stepSize != 0)
           {
               stepsCounter++;
           }

            return stepsCounter;
        }

        public void PrintTest(TestType testType = TestType.Positive)
        {
            int[][] cases = null;
            switch (testType)
            {
                case TestType.Positive:
                    cases = new int[][]
                    {
                        new int[] {10, 85, 30},
                        new int[] {10, 8500, 30},
                        new int[] {1, 85, 1},
                        new int[] { 55, 1000000000, 40 },
                        new int[] { 1000000000, 1000000000, 40 }
                    };

                    break;
                case TestType.Negative:                    
                    cases = new int[][] {
                        new int[] {100, 1000000001, 10},
                        new int[] { 1000000001, 100, 10},
                        new int[] {100, 100, 1000000001},
                        new int[] { 1000000000, 99999, 10},
                        new int[] { 0, 1, 1},
                        new int[] { 1, 0, 1},
                        new int[] { 1, 1, 0}
                    };
                    
                    break;
            }

            foreach (var item in cases)
            {
                try
                {
                    PrintInputData(item[0], item[1], item[2]);
                    PrintResult(Count(item[0], item[1], item[2]));
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine("\n{0}", ex.Message);
                }
            }
        }

        private void PrintInputData(int startPosition, int desiredPosition, int stepSize)
        {
            Console.WriteLine("\nThe start frog's position is {0}", startPosition);
            Console.WriteLine("The desired frog's position is {0}", desiredPosition);
            Console.WriteLine("The frog's jump size is {0}", stepSize);
        }

        private void PrintResult(int stepNumber)
        {
            Console.WriteLine("The frog needs to jump {0} times.\n", stepNumber);
        }

        private void ValidateParameters(int X, int Y)
        {
            if (X > Y)
            {
                throw new ArgumentOutOfRangeException(nameof(X),
                  "According to the task assumptions X<=Y");
            }
        }
    }
}
