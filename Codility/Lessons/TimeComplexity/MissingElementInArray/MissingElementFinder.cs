﻿using System;
using System.Collections.Generic;
using System.Linq;
using Codility.Helpers;
using Codility.Testing;
using Codility.Validation;

namespace Codility.Lessons.TimeComplexity.MissingElementInArray
{
    public class MissingElementFinder: Validator, ITestPrinter
    {
        public int Find(int[] A)
        {
            ValidateParameterForNull(A);
            if (A.Length < 2)
            {
                throw new ArgumentOutOfRangeException(nameof(A),
                       A,
                     "According to the task assumptions the array contains integers in the range [1..(N + 1)]. Thus, 2 is the minimum length of array to follow this assumption");
            }

            ValidateElementForRange(A.Max(), 100000);
            ValidateElementForRange(A.Min(), 100000);
            ValidateDistinctElements(A);

            var missingElements = new List<int>();

            // Check an array, whether it contains the missing element.
            bool found = false;

            if (A.Length == 2)
            {
                if (A[1] - A[0] != 1)
                {
                    return A[0] + 1;
                }
            }

            for (int i = 0, j = A.Length - 1; i < A.Length / 2; i++, j--)
            {
                if (A[j] - A[j - 1] != 1)
                {
                    found = true;

                    if (A[j - 1] - A[j - 2] == 1)
                    {
                        missingElements.Add(A[j - 1] + 1);
                    }
                    else
                    {
                        missingElements.Add(A[j] - 1);
                    }
                }

                if (A[i + 1] - A[i] != 1)
                {
                    found = true;

                    if (A[i + 2] - A[i + 1] == 1)
                    {
                        missingElements.Add(A[i + 1] - 1);
                    }
                    else
                    {
                        missingElements.Add(A[i + 2] - 1);
                    }
                }
            }

            if (!found)
            {
                throw new ArgumentOutOfRangeException(nameof(A),
                       A,
                     "According to the task assumptions minimum one element is missing.");
            }

            if (missingElements.Distinct().Count() > 1)
            {
                throw new ArgumentOutOfRangeException(nameof(A),
                       A,
                     "According to the task assumptions each element of array A is an integer within the range [1..(N + 1)].");
            }

            return missingElements.FirstOrDefault();
        }

        public void PrintTest(TestType testType = TestType.Positive)
        {
            int[][] cases = null;

            switch(testType)
            {
                case TestType.Positive:
                    cases = new int[][] {
                        new int[] { 0, 2, 3 },
                        new int[] { 1, 0, 3 },
                        new int[] { 1, 2, 0 },
                        new int[] { 1, 9, 10, 11 },
                        new int[] { 8, 1, 10, 11 },
                        new int[] { 8, 9, 1, 11 },
                        new int[] { 8, 9, 10, 1},
                        new int[] { 1, 9, 10, 11, 12 },
                        new int[] { 8, 1, 10, 11, 12 },
                        new int[] { 8, 9, 1, 11, 12 },
                        new int[] { 8, 9, 10, 1, 12},
                        new int[] { 8, 9, 10, 11, 1},
                        new int[] { 17, 1, 2, 3},
                        new int[] { 17, 2, 3, 4},
                        new int[] { 1, 0, 3 },
                        new int[] { 8, 9, 1, 11, 12 },
                        new int[] { 1, 0},
                        new int[] { 0, 15},
                        new int[] { 99999, 100000, 10000}
                    };
                    break;

                case TestType.Negative:
                    cases = new int[][] {
                        new int[] { 8, 9, 3, 11, 12, 1, 14},
                        new int[] { 0, 1, 2, 7, 8, 9 },
                        new int[] { },
                        new int[] { 1, 5, 7, 9, 12 },
                        new int[] { 0, 1, 2, 3},
                        new int[] { 0, 1, 2},
                        new int[] { -1, 0 },
                        new int[] { 99999, 100000, 100001, 3 },
                        new int[] { 0, 1},
                    };
                    break;
            }

            foreach (var item in cases)
            {
                try
                {
                    ArrayHelper.PrintInitialArray(item);
                    Console.WriteLine(string.Format("Missing element is {0}",
                                                     Find(item)));
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    
                    Console.WriteLine("\n{0}", ex.Message);
                }
            }
        }
    }
}
