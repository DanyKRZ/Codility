﻿using System;
using System.Linq;
using Codility.Helpers;
using Codility.Testing;
using Codility.Validation;

namespace Codility.Lessons.TimeComplexity.TapeEquilibrium
{
    public class TapeEquilibriumCounter: Validator, ITestPrinter
    {
        public int Count(int[] A)
        {
            ValidateParameterForNull(A);
            ValidateArrayLengthRange(A.Length, 100000, 2);
            ValidateElementForRange(A.Min(), 1000, -1000);
            ValidateElementForRange(A.Max(), 1000, -1000);

            int aSum = A.First();
            int pSum = A.Sum() - A.First();
            int minimumElement = Math.Abs(aSum - pSum);

            for (int i = 1; i < A.Length - 1; i++)
            {
                aSum += A[i];
                pSum -= A[i];
                int element = Math.Abs(aSum - pSum);

                minimumElement = element < minimumElement ?
                                    element :
                                    minimumElement;

                if (minimumElement == 0)
                {
                    return 0;
                }
            }

            return minimumElement;
        }

        public void PrintTest(TestType testType = TestType.Positive)
        {
            int[][] cases = null;

            switch (testType)
            {
                case TestType.Positive:
                    cases = new int[][] {
                        new int[] { 3, 1, 2, 4, 3}, // 1
                        new int[] { 3, 1}, // 2
                        new int[] { 3, 1, 2}, //0
                        new int[] { 3, 1, 2, 5}, //1
                        new int[] { 3, 1, 2, 4, 3, 7} //0
                    };
                    break;
                case TestType.Negative:
                    cases = new int[][] {
                        ArrayHelper.GenerateRandomArray(100001, 1000),
                        new int[] { 1, 1000001},
                        new int[] { 1, -1000001}
                    };
                    break;
            }

            foreach (var item in cases)
            {
                try
                {
                    ArrayHelper.PrintInitialArray(item);
                    Console.WriteLine(Count(item));
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine("\n{0}", ex.Message);
                }
            }
        }
    }
}
