﻿A non-empty array A consisting of N integers is given. 
Array A represents numbers on a tape.

Any integer P, such that 0 < P < N, splits this tape into two non-empty parts:
A[0], A[1], ..., A[P − 1] 
and A[P], A[P + 1], ..., A[N − 1].

The difference between the two parts is the value of:
|(A[0] + A[1] + ... + A[P − 1]) − (A[P] + A[P + 1] + ... + A[N − 1])|

In other words, it is the absolute difference between the sum of the first part and the sum of the second part.

Look for the examples below:

  A[0] = 3		A[0] = 3   	A[0] = 3		A[0] = 3	A[0] = 3
  A[1] = 1		A[1] = 1	A[1] = 1		A[1] = 1	A[1] = 1
				A[2] = 2	A[2] = 2		A[2] = 2	A[2] = 2
  3 - 1 = 2					A[3] = 5		A[3] = 4	A[3] = 4
				3 - 3 = 0					A[4] = 3	A[4] = 3			
				4 - 2 = 2	3 - 8 = 5					A[5] = 7
							4 - 7 = 3		3-10
							6 - 5 = 1		4-9			3 - 17 = 14
											6-7			4 - 16 = 12
											10-3		6 - 14 = 8
														10 - 10 = 0
  														13 - 7 = 6
  
  Write a function:

class Solution { public int solution(int[] A); }

that, given a non-empty array A of N integers, returns the minimal 
difference that can be achieved.

For example, given:

  A[0] = 3
  A[1] = 1
  A[2] = 2
  A[3] = 4
  A[4] = 3

the function should return 1, as explained above.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [2..100,000];
each element of array A is an integer within the range [−1,000..1,000].