﻿using System;
using System.Linq;

namespace Codility.Validation
{
    public class Validator
    {
        public void ValidateArrayLengthRange(int length, int maxLength, int minLength = 0)
        {
            if (length > maxLength || length < minLength)
            {
                throw new ArgumentOutOfRangeException(nameof(length),
                    length,
                  string.Format("According to the task assumptions the array length is within the range [{0}..{1}]", minLength, maxLength));
            }
        }

        public void ValidateArrayLengthForOdd(int length, bool isOdd)
        {
            if (length % 2 == 0 && isOdd)
            {
                throw new ArgumentOutOfRangeException(nameof(length),
                    length,
                  string.Format("According to the task assumptions the array length number is odd."));
            }

            if (length % 2 != 0 && !isOdd)
            {
                throw new ArgumentOutOfRangeException(nameof(length),
                    length,
                  string.Format("According to the task assumptions the array length number is not odd."));
            }
        }

        public void ValidateDistinctElements(int[] array)
        {
            if (array.Distinct().Count() != array.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(array),
                    array,
                    "According to the task assumptions the elements of A are all distinct");
            }
        }

        public void ValidateElementForRange(int element, int max, int min = 0)
        {
            string errorString = $"According to the task assumptions the element is an integer within the range [{min}..{max}].";

            if (element < min || element > max)
            {
                throw new ArgumentOutOfRangeException(nameof(element),
                    element,
                    errorString);
            }
        }

        public void ValidateParameterForNull<T>(T parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException(nameof(parameter));
            }
        }
    }
}
