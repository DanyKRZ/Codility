﻿using System;
using System.Text;

namespace Codility.Helpers
{
    public static class ArrayHelper
    {
        public static void Print(this int[] A)
        {
            foreach (int i in A)
            {
                Console.Write("{0} ", i);
            }
        }

        public static int[] GenerateRandomArray(int arrayLength, int maxElement, int minElement = 0)
        {
            var random = new Random();
            int[] array = new int[arrayLength];

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = random.Next(minElement, maxElement);
            }

            return array;
        }

        public static string ToString(this int[] array, string separator)
        {
            StringBuilder arr = new StringBuilder();

            foreach (int i in array)
            {
                arr.Append($"{i}{separator}");
            }

            return arr.ToString();
        }

        public static void PrintInitialArrayAndResult(int[] array, string result)
        {
            PrintInitialArray(array);
            Console.WriteLine(result);
        }

        public static void PrintInitialArray(int[] array)
        {
            Console.WriteLine(string.Format("\nThe initial array is: {0}", array.ToString(" ")));
        }

    }
}
