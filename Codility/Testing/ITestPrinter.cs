﻿namespace Codility.Testing
{
    public interface ITestPrinter
    {
        public void PrintTest(TestType testType = TestType.Positive);
    }

    public enum TestType
    {
        Positive,
        Negative,
    }
}
