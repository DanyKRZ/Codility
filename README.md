This repository was created to track the progress of completing the developers tasks from Codility platform (https://app.codility.com).

Codility is technical interview platform for teams to test the coding skills of developers and make evidence-based hiring decisions.

