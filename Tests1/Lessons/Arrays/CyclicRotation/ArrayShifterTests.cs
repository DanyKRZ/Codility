﻿using Codility.Helpers;
using System;
using Codility.Lessons.Arrays.CyclicRotation;
using Xunit;

namespace Codility.Tests.Lessons.Arrays.CyclicRotation
{
    public class ArrayShifterTests
    {     
        [Theory]
        [InlineData(new int[] { 3, 8, 9, 7, 6 }, 
                    1, 
                    new int[] { 6, 3, 8, 9, 7 })]
        [InlineData(new int[] { -3, 8, 9, 7, 6 },
                    1,
                    new int[] { 6, -3, 8, 9, 7 })]
        [InlineData(new int[] { 3, 8, 9, 7, 6 }, 
                    2,
                    new int[] { 7, 6, 3, 8, 9 })]
        [InlineData(new int[] { 3, 8, 9, 7, 6 },
                    3,
                    new int[] { 9, 7, 6, 3, 8 })]
        [InlineData(new int[] { -1000, 8, 9, 7, 6 },
                    1,
                    new int[] { 6, -1000, 8, 9, 7 })]
        [InlineData(new int[] { 1000, 8, 9, 7, 6 },
                    1,
                    new int[] { 6, 1000, 8, 9, 7 })]
        public void ShiftArray_ReturnsShiftedArray(
            int[] arrayToShift,
            int shiftingTimes,
            int[] expectedShiftedArray)
        {
            var arrayShifter = new ArrayShifter();

            var actualShiftedArray = arrayShifter.Shift(arrayToShift, shiftingTimes);

            Assert.Equal(expectedShiftedArray, actualShiftedArray);
        }

        [Theory]
        [InlineData(new int[] { 1, 2, 3, 4 },
                    4, 
                    new int[] { 1, 2, 3, 4 })]
        [InlineData(new int[] { 0, 1, 0 }, 
                    3,
                    new int[] { 0, 1, 0 })]
        [InlineData(new int[] { 7, 8 },
                    2,
                    new int[] { 7, 8 })]
        [InlineData(new int[] { -4 },
                    1,
                    new int[] { -4 })]
        [InlineData(new int[] {  },
                    0,
                    new int[] {  })]
        public void ShiftArray_AsManyTimesAsItsLength_ReturnsTheSameArray(
            int[] arrayToShift,
            int shiftingTimes,
            int[] expectedShiftedArray)
        {
            var arrayShifter = new ArrayShifter();

            var actualShiftedArray = arrayShifter.Shift(arrayToShift, shiftingTimes);

            Assert.Equal(expectedShiftedArray, actualShiftedArray);
        }
        
        [Theory]
        [InlineData(new int[] { 3, 8, 9, 7, 6 }, -1)]
        [InlineData(new int[] { -1001, 8, 9, 7, 6 }, 1)]
        [InlineData(new int[] { 1001, 8, 9, 7, 6 }, 1)]
        public void ShiftArray_WithOutOfRangeParameters_ReturnsArgumentRangeException(
            int[] arrayToShift,
            int shiftingTimes)
        {
            var arrayShifter = new ArrayShifter();

            Action shift = () => arrayShifter.Shift(arrayToShift, shiftingTimes);

            Assert.Throws<ArgumentOutOfRangeException>(shift);
        }

        [Fact]
        public void ShiftArray_OfOutOfRangeLength_ReturnsArgumentRangeException()
        {
            var arrayShifter = new ArrayShifter();
            var outOfRangeLength = 101;
            var arrayToShift = ArrayHelper.GenerateRandomArray(outOfRangeLength, 1000, -1000);

            Action shift = () => arrayShifter.Shift(arrayToShift, 1);

            Assert.Throws<ArgumentOutOfRangeException>(shift);
        }

        [Fact]
        public void Shift_WithNullParameter_ThrowsArgumentNullException()
        {
            var shifter = new ArrayShifter();
            int timesToShift = 1;

            Action shift = () => shifter.Shift(null, timesToShift);

            Assert.Throws<ArgumentNullException>(shift);
        }
    }
}
