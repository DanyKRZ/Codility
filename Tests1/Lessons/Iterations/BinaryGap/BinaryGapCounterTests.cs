﻿using System;
using Codility.Lessons.Iterations.BinaryGap;
using Xunit;

namespace Codility.Tests.Lessons.Iterations.BinaryGap
{
    public class BinaryGapCounterTests
    {
        [Theory]
        [InlineData (9, 2)]
        [InlineData (10, 1)]
        [InlineData (169, 2)]
        [InlineData (17, 3)]
        [InlineData (33, 4)]
        [InlineData (65, 5)]
        [InlineData (69, 3)]        
        [InlineData (1041, 5)]
        [InlineData(5, 1)]
        [InlineData(1, 0)]
        [InlineData(2147483647, 0)]
        public void Count_ReturnsMaximumBinaryGapsNumber(int numberToCountGaps, int expectedGapsNumber)
        {
            var counter = new BinaryGapCounter();

            int actualGapsNumber = counter.Count(numberToCountGaps);

            Assert.Equal(expectedGapsNumber, actualGapsNumber);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void Count_OutOfRangeParameters_ThrowsArgumentOutOfRangException(int numberToCountGaps)
        {
            var counter = new BinaryGapCounter();

            Action countGaps = () => counter.Count(numberToCountGaps);

            Assert.Throws<ArgumentOutOfRangeException>(countGaps);
        }
    }
}