﻿using System;
using Codility.Lessons.TimeComplexity.FrogJmp;
using Xunit;

namespace Codility.Tests.Lessons.TimeComplexity.FrogJmp
{
    public class FrogJumpsAcrossTheRoadCounterTests
    {
        [Theory]
        [InlineData(10, 85, 30, 3)]
        [InlineData(10, 8500, 30, 283)]
        [InlineData(1, 85, 1, 84)]
        [InlineData(55, 1000000000, 40, 24999999)]
        [InlineData(1000000000, 1000000000, 40, 0)]
        public void Count_ReturnsNumberOfStepsToReachPosition(int startPosition,
            int desiredPosition,
            int stepSize,
            int expectedStepNumber)
        {
            var counter = new FrogJumpsAcrossTheRoadCounter();

            var actualStepNumber = counter.Count(startPosition, desiredPosition, stepSize);

            Assert.Equal(expectedStepNumber, actualStepNumber);
        }

        [Theory]
        [InlineData(100, 1000000001, 10)]
        [InlineData(1000000001, 100, 10)]
        [InlineData(100, 100, 1000000001)]
        [InlineData(1000000000, 99999, 10)]
        [InlineData(0, 1, 1)]
        [InlineData(1, 0, 1)]
        [InlineData(1, 1, 0)]
        public void Count_WithOutOfRangeParameters_ThrowsArgumentOutOfRangeException(int startPosition,
            int desiredPosition,
            int stepSize)
        {
            var counter = new FrogJumpsAcrossTheRoadCounter();

            Action countSteps = () => counter.Count(startPosition, desiredPosition, stepSize);

            Assert.Throws<ArgumentOutOfRangeException>(countSteps);        
        }
    }
}
