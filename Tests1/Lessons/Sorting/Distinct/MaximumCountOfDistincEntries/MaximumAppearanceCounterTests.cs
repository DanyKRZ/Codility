﻿using System;
using Codility.Helpers;
using Codility.Lessons.Sorting.Distinct.MaximumCountOfDistinctEntries;
using Xunit;

namespace Codility.Tests.Lessons.Sorting.Distinct.MaximumCountOfDistincEntries
{
    public class MaximumAppearanceCounterTests
    {
        [Theory]
        [InlineData(new int[] { 1, 2, 3, 4, 5 }, 1)]
        [InlineData(new int[] { 2, 2, 3, 4, 5 }, 2)]
        [InlineData(new int[] { 1, 1, 1, 3, 1, 1, 3, 0, 1, 4, 3, 0, 0, 0, 0 }, 6)]
        [InlineData(new int[] { 2, -1, -9, -8, -5, 1, -2, 0, -10, 5, 8, -4, 7, 5, -1 }, 2)]
        [InlineData(new int[] { 7, 7, 7, 7, 7, 7, 7, 7, 7, 7 }, 10)]
        public void Count_ReturnsNumberOfDistinctArrayElements(int[] array,
            int expectedDistinctElements)
        {
            var counter = new MaximumAppearanceCounter();

            var actualDistincElements = counter.Count(array);

            Assert.Equal(expectedDistinctElements, actualDistincElements);
        }

        [Theory]
        [InlineData(new int[] { -1000001, 8, 9, 7, 6 })]
        [InlineData(new int[] { 1000001, 8, 9, 7, 6 })]
        public void Count_WithOutOfRangeParameters_ReturnsArgumentRangeException(int[] array)
        {
            var counter = new MaximumAppearanceCounter();

            Action shift = () => counter.Count(array);

            Assert.Throws<ArgumentOutOfRangeException>(shift);
        }

        [Fact]
        public void Count_OfOutOfRangeArrayLength_ReturnsArgumentRangeException()
        {
            var counter = new MaximumAppearanceCounter();
            var outOfRangeLength = 100001;
            var array = ArrayHelper.GenerateRandomArray(outOfRangeLength, 1000, -1000);

            Action count = () => counter.Count(array);

            Assert.Throws<ArgumentOutOfRangeException>(count);
        }

        [Fact]
        public void Count_WithNullParameter_ThrowsArgumentNullException()
        {
            var counter = new MaximumAppearanceCounter();

            Action count = () => counter.Count(null);

            Assert.Throws<ArgumentNullException>(count);
        }
    }
}
