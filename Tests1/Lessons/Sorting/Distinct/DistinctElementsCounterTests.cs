﻿using System;
using Codility.Helpers;
using Codility.Lessons.Sorting.Distinct;
using Xunit;

namespace Codility.Tests.Lessons.Sorting.Distinct
{
    public class DistinctElementsCounterTests
    {
        [Theory]
        [InlineData(new int[] {1, 2, 3, 4, 5}, 5)]
        [InlineData(new int[] { 3, 1, 1, 3, 0, 2, 0, 2, 1, 3, 1, 2, 1, 0, 3 }, 4)]
        [InlineData(new int[] { -5, -7, 3, -1, -8, -8, 2, -6, 8, 5, 9, -7, -8, -5, 9, }, 10)]
        [InlineData(new int[] { 7, 7, 7, 7, 7, 7 }, 1)]
        [InlineData(new int[] { -1000000, 1000000, 7, 7, 7, 7 }, 3)]
        public void Count_ReturnsNumberOfDistinctArrayElements(int[] array,
            int expectedDistinctElements)
        {
            var counter = new DistinctElementsCounter();

            var actualDistincElements = counter.Count(array);

            Assert.Equal(expectedDistinctElements, actualDistincElements);
        }

        [Theory]
        [InlineData(new int[] { -1000001, 8, 9, 7, 6 })]
        [InlineData(new int[] { 1000001, 8, 9, 7, 6 })]
        public void Count_WithOutOfRangeParameters_ReturnsArgumentRangeException(int[] array)
        {
            var counter = new DistinctElementsCounter();

            Action count = () => counter.Count(array);

            Assert.Throws<ArgumentOutOfRangeException>(count);
        }

        [Fact]
        public void Count_OfOutOfRangeArrayLength_ReturnsArgumentRangeException()
        {
            var counter = new DistinctElementsCounter();
            var outOfRangeLength = 100001;
            var array = ArrayHelper.GenerateRandomArray(outOfRangeLength, 1000, -1000);

            Action shift = () => counter.Count(array);

            Assert.Throws<ArgumentOutOfRangeException>(shift);
        }

        [Fact]
        public void Count_WithNullParameter_ThrowsArgumentNullException()
        {
            var counter = new DistinctElementsCounter();

            Action count = () => counter.Count(null);

            Assert.Throws<ArgumentNullException>(count);
        }
    }
}
