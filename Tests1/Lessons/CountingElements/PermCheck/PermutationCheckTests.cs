﻿using System;
using Codility.Lessons.CountingElements.PermCheck;
using Xunit;

namespace Codility.Tests.Lessons.CountingElements.PermCheck
{
    public class PermutationCheckTests
    {
        [Theory]
        [InlineData(new int[] { 4, 1, 3, 2 }, 1)]
        [InlineData(new int[] { 1 }, 1)]
        [InlineData(new int[] { 1, 2 }, 1)]
        [InlineData(new int[] { 9, 7, 5, 3, 2, 1, 4, 6, 10, 8, 11 }, 1)]
        public void Check_ReturnsPositivePermutationResult(int[] array,
           int expectedPermutationResult)
        {
            var checker = new PermutationChecker();

            int actualPermutationResult = checker.Check(array);

            Assert.Equal(expectedPermutationResult, actualPermutationResult);
        }

        [Theory]
        [InlineData(new int[] { 100000000 }, 0)]
        [InlineData(new int[] { 2 }, 0)]
        [InlineData(new int[] { 4, 1, 3 }, 0)]
        [InlineData(new int[] { 4, 3 }, 0)]
        [InlineData(new int[] { 1, 3 }, 0)]
        [InlineData(new int[] { 1, 1 }, 0)]
        [InlineData(new int[] { 1, 4, 1 }, 0)]
        [InlineData(new int[] { 2, 2, 2 }, 0)]
        [InlineData(new int[] { 9, 5, 7, 3, 2, 7, 3, 1, 10, 8 }, 0)]
        [InlineData(new int[] { 9, 5, 7, 3, 2}, 0)]
        public void Check_ReturnsNegativePermutationResult(int[] array,
           int expectedPermutationResult)
        {
            var checker = new PermutationChecker();

            int actualPermutationResult = checker.Check(array);

            Assert.Equal(expectedPermutationResult, actualPermutationResult);
        }

        [Theory]
        [InlineData(new int[] { 100000001, 1})]
        [InlineData(new int[] { 0, 1 })]
        [InlineData(new int[] { 1, -1 })]
        public void Count_WithOutOfRangeParameters_ThrowsArgumentOutOfRangeException(int[] arrayToCheck)
        {
            var checker = new PermutationChecker();

            Action check = () => checker.Check(arrayToCheck);

            Assert.Throws<ArgumentOutOfRangeException>(check);
        }

        [Fact]
        public void Count_WithNullParameter_ThrowsArgumentNullException()
        {
            var checker = new PermutationChecker();

            Action check = () => checker.Check(null);

            Assert.Throws<ArgumentNullException>(check);
        }
    }
}
