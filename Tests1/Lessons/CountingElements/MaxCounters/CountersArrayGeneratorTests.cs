﻿using System;
using Codility.Lessons.CountingElements.MaxCounters;
using Xunit;

namespace Codility.Tests.Lessons.CountingElements.MaxCounters
{
    public class CountersArrayGeneratorTests
    {
        [Theory]
        [InlineData(new int[] { 3, 4, 4, 6, 1, 4, 4 }, 
                    5, 
                    new int[] { 3, 2, 2, 4, 2 })]
        [InlineData(new int[] { 3, 4, 4, 6, 1, 4, 4, 3, 4, 4, 6, 1, 4, 4 }, 
                    5, 
                    new int[] { 7, 6, 6, 8, 6 })]
        [InlineData(new int[] { 3, 4, 4, 4, 4, 4, 6 }, 
                    5,
                    new int[] { 5, 5, 5, 5, 5 })]
        [InlineData(new int[] { 3, 4, 4, 4, 4, 4, 3, 4, 4, 4, 4, 4, 6 }, 
                    5,
                    new int[] { 10, 10, 10, 10, 10 })]
        [InlineData(new int[] { 1, 1, 2 },
                    1,
                    new int[] { 2 })]
        [InlineData(new int[] { 1, 7 }, 
                    6,
                    new int[] { 1, 1, 1, 1, 1, 1 })]
        [InlineData(new int[] { 1, 3, 7 },
                    6,
                    new int[] { 1, 1, 1, 1, 1, 1 })]
        [InlineData(new int[] { 21, 7, 14, 21, 14, 21, 18, 5, 5, 21, 14, 7, 6, 21, 6, 14, 18, 
                                15, 4, 10, 19, 5, 10, 10, 12, 10, 17, 4, 16, 21 }, 
                    20, 
                    new int[] { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9  })]
        [InlineData(new int[] { 14, 32, 14, 51, 60, 85, 91, 99, 24, 28, 14, 3, 22, 101, 99, 2,
                                70, 19, 91, 64, 18, 10, 101, 68, 53, 42, 78, 28, 13, 47, 37, 101, 67, 12, 27, 101, 
                                17, 101, 101, 77, 24, 42, 101, 40, 14, 4, 101, 13, 101, 101 },
                    100,
                    new int[] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
                                10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
                                10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
                                10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
                                10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 })]
        public void Generate_ReturnsCorrectArray(int[] incomingArray,
            int expectedLengthForGeneratedArray,
            int[] expectedGeneratedArray)
        {
            var generator = new CountersArrayGenerator();

            var actualGeneratedArray = generator.Generate(expectedLengthForGeneratedArray, incomingArray);

            Assert.Equal(expectedGeneratedArray, actualGeneratedArray);
        }

        [Theory]
        [InlineData(new int[] { 100000, 1 }, 5)]
        [InlineData(new int[] { 6, 0 }, 5)]
        [InlineData(new int[] { 1, 2, 3, 4, 5, 7 }, 5)]
        [InlineData(new int[] {  }, 5)]
        public void Generate_WithOutOfRangeParameters_ThrowsArgumentOutOfRangeException(int[] incomingArray,
            int expectedLengthForGeneratedArray)
        {
            var generator = new CountersArrayGenerator();

            Action generate = () => generator.Generate(expectedLengthForGeneratedArray, incomingArray);

            Assert.Throws<ArgumentOutOfRangeException>(generate);
        }

        [Fact]
        public void Count_WithNullParameter_ThrowsArgumentNullException()
        {
            var generator = new CountersArrayGenerator();

            Action generate = () => generator.Generate(1, null);

            Assert.Throws<ArgumentNullException>(generate);
        }

        [Fact]
        public void Count_WithOutOfRangeArrayLength_ThrowsArgumentOutOfRangeException()
        {
            var generator = new CountersArrayGenerator();
            int [] array = new int[100001];

            Action generate = () => generator.Generate(1, array);

            Assert.Throws<ArgumentOutOfRangeException>(generate);
        }
    }
}

