﻿using System;
using Codility.Lessons.CountingElements.FrogRiverOne;
using Xunit;

namespace Codility.Tests.Lessons.CountingElements.FrogRiverOne
{
    public class FrogJumpsAcrossTheRiverCounterTests
    {
        [Theory]
        [InlineData(5, new int[] { 1, 3, 1, 4, 2, 3, 5, 4 }, 6)]
        [InlineData(5, new int[] { 1, 3, 1, 4, 2, 3, 4, 4 }, -1)]
        [InlineData(5, new int[] { 5, 4, 3, 4, 3, 2, 1 }, 6)]
        [InlineData(1, new int[] { 1 }, 0)]
        [InlineData(100000, new int[] { 5, 4, 3, 4, 3, 2, 1 }, -1)]
        [InlineData(100000, new int[] { 5, 4, 100000, 4, 3, 2, 1 }, -1)]
        public void Count_ReturnsNumberOfStepsToReachPosition(int lastRiverPosition,
            int[] leaves,
            int expectedEarliestTime)
        {
            var counter = new FrogJumpsAcrossTheRiverCounter();

            var actualEarliestTime = counter.Count(lastRiverPosition, leaves);

            Assert.Equal(expectedEarliestTime, actualEarliestTime);
        }

        [Theory]
        [InlineData(100001, new int[] { 1, 3, 1, 4, 2, 3, 5, 4 })]
        [InlineData(100001, new int[] { 1, 3, 100001, 4, 2, 3, 4, 4 })]
        [InlineData(5, new int[] { 1, 3, 1, 4, 100001, 3, 5, 4 })]
        [InlineData(0, new int[] { 5, 5, 3, 4, 3, 2, 1 })]
        [InlineData(5, new int[] { 5, 5, 3, 0, 3, 2, 1 })]
        public void Count_WithOutOfRangeParameters_ThrowsArgumentOutOfRangeException(int lastRiverPosition,
            int[] leaves)
        {
            var counter = new FrogJumpsAcrossTheRiverCounter();

            Action count = () => counter.Count(lastRiverPosition, leaves);

            Assert.Throws<ArgumentOutOfRangeException>(count);
        }

        [Fact]
        public void Count_WithNullParameter_ThrowsArgumentNullException()
        {
            var counter = new FrogJumpsAcrossTheRiverCounter();
            int lastRiverPosition = 1;            

            Action count = () => counter.Count(lastRiverPosition, null);

            Assert.Throws<ArgumentNullException>(count);
        }
    }
}
