﻿using Codility.Helpers;
using System;
using System.Text;
using Codility.Lessons.PrefixSums.GenomicRangeQuery;
using Xunit;

namespace Codility.Tests.Tasks.NucleotidesImpactFactor
{
    public class MinimalImpactFactorFinderTests
    {
        [Theory]
        [InlineData("CAGCCTA", new int[] { 2, 5, 0}, new int[] { 4, 5, 6 }, new int[] { 2, 4, 1 })]
        [InlineData("CAGCCTA", new int[] { 1, 2, 0, 2, 4 }, new int[] { 4, 6, 3, 5, 6 }, new int[] { 1, 1, 1, 2, 1 })]
        [InlineData("GTATA", new int[] { 0, 1, 2, 0, 3 }, new int[] { 3, 4, 2, 4, 3 }, new int[] { 1, 1, 1, 1, 4 })]
        [InlineData("A", new int[] { 0 }, new int[] { 0 }, new int[] { 1 })]
        [InlineData("AA", new int[] { 0, 1 }, new int[] { 1, 1 }, new int[] { 1, 1 })]
        [InlineData("AAA", new int[] { 0, 0, 0 }, new int[] { 1, 1, 1 }, new int[] { 1, 1, 1 })]
        public void Find_ReturnsSmallestNonOccurredElementOfArray(string nucleotides,
            int[] P,
            int[] Q,
            int[] expectedMinimalImpactFactor)
        {
            var finder = new MinimalImpactFactorFinder();

            int[] actualMinimalImpactFactor = finder.Find(nucleotides, P, Q);

            Assert.Equal(expectedMinimalImpactFactor, actualMinimalImpactFactor);
        }

        [Theory]
        [InlineData("X", new int[] { 2, 5, 0 }, new int[] { 4, 5, 6 })]
        [InlineData("C", new int[] { 2, 5, 0, 0 }, new int[] { 4, 5, 6 })]
        [InlineData("C", new int[] { 2, 5, 0 }, new int[] { 4, 5, 6, 0 })]
        [InlineData("C", new int[] { 2, 5, 0 }, new int[] { 4, 5, 6 })]
        [InlineData("GTA", new int[] { 2, 1, 0 }, new int[] { 1, 2, 0 })]
        public void Find_WithOutOfRangeParameters_ReturnsArgumentRangeException(string nucleotides,
            int[] P,
            int[] Q)
        {
            var finder = new MinimalImpactFactorFinder();

            Action find = () => finder.Find(nucleotides, P, Q);

            Assert.Throws<ArgumentOutOfRangeException>(find);
        }

        [Theory]
        [InlineData("C", null, new int[] { 4, 5, 6 })]
        [InlineData(null, new int[] { 2, 5, 0 }, new int[] { 4, 5, 6 })]
        [InlineData("C", new int[] { 2, 5, 0 }, null)]
        public void Find_WithOutOfRangeParameters_ReturnsArgumentNullException(string nucleotides,
            int[] P,
            int[] Q)
        {
            var finder = new MinimalImpactFactorFinder();

            Action find = () => finder.Find(nucleotides, P, Q);

            Assert.Throws<ArgumentNullException>(find);
        }

        [Fact]
        public void Find_WithOutOfRangeNucleotidesStringLength_ReturnsArgumentRangeException()
        {
            var finder = new MinimalImpactFactorFinder();

            StringBuilder nucleotidesString = new StringBuilder();
            var outOfRangeNucleotidesStringLength = 100001;

            var pImpactFactors = new int[] {2, 5, 0};
            var qImpactFactors = new int[] {4, 5, 6};

            for (int i = 0; i <= outOfRangeNucleotidesStringLength; i++)
            {
                nucleotidesString.Append('A');
            }

            Action find = () => finder.Find(nucleotidesString.ToString(), pImpactFactors, qImpactFactors);

            Assert.Throws<ArgumentOutOfRangeException>(find);
        }

        [Fact]
        public void Find_WithOutOfRangePImpactFactorsLength_ReturnsArgumentRangeException()
        {
            var finder = new MinimalImpactFactorFinder();

            int outOfRangeImpactFactorsLength = 50001;
            string nucleotidesString = "ATCG";

            var pImpactFactors = ArrayHelper.GenerateRandomArray(outOfRangeImpactFactorsLength, 3);
            var qImpactFactors = new int[] { 4, 5, 6 };

            Action find = () => finder.Find(nucleotidesString, pImpactFactors, qImpactFactors);

            Assert.Throws<ArgumentOutOfRangeException>(find);
        }

        [Fact]
        public void Find_WithOutOfRangeQImpactFactorsLength_ReturnsArgumentRangeException()
        {
            var finder = new MinimalImpactFactorFinder();

            int outOfRangeImpactFactorsLength = 50001;
            string nucleotidesString = "ATCG";

            var pImpactFactors = new int[] { 4, 5, 6 };
            var qImpactFactors = ArrayHelper.GenerateRandomArray(outOfRangeImpactFactorsLength, 3);
            
            Action find = () => finder.Find(nucleotidesString, pImpactFactors, qImpactFactors);

            Assert.Throws<ArgumentOutOfRangeException>(find);
        }
    }
}

